fn main() {

    println!("Hello")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_something() {
        println!("In test")
    }
}
